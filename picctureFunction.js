// Add event listeners to all images in the document
document.querySelectorAll("img").forEach((img) => {
    img.addEventListener("click", function () {
        openImageModal(img.src, img.alt);
    });
});

// Function to create and display the image modal
function openImageModal(src, alt) {
    // Create overlay
    const overlay = document.createElement("div");
    overlay.id = "imageOverlay";
    overlay.style.position = "fixed";
    overlay.style.top = "0";
    overlay.style.left = "0";
    overlay.style.width = "100%";
    overlay.style.height = "100%";
    overlay.style.backgroundColor = "rgba(0, 0, 0, 0.8)";
    overlay.style.display = "flex";
    overlay.style.justifyContent = "center";
    overlay.style.alignItems = "center";
    overlay.style.overflow = "hidden"; // Prevent overflow issues
    overlay.style.zIndex = "1000";

    // Create image element
    const image = document.createElement("img");
    image.src = src;
    image.alt = alt;
    image.style.maxWidth = "90%";
    image.style.maxHeight = "90%";
    image.style.border = "2px solid white";
    image.style.transform = "scale(1)";
    image.style.position = "relative";
    image.style.transition = "transform 0.2s";

    // Track zoom level and drag state
    let zoomLevel = 1;
    let isDragging = false;
    let startX = 0;
    let startY = 0;
    let offsetX = 0;
    let offsetY = 0;

    // Add event listener for scroll to zoom in/out
    overlay.addEventListener("wheel", (event) => {
        event.preventDefault(); // Prevent page scroll
        if (event.deltaY < 0) {
            // Scroll up: Zoom in
            zoomLevel = Math.min(zoomLevel + 0.1, 5); // Max zoom level: 5x
        } else {
            // Scroll down: Zoom out
            zoomLevel = Math.max(zoomLevel - 0.1, 1); // Min zoom level: 1x
        }
        image.style.transform = `scale(${zoomLevel}) translate(${offsetX / zoomLevel}px, ${offsetY / zoomLevel}px)`;
    });

    // Add mouse down event for dragging
    image.addEventListener("mousedown", (event) => {
        if (zoomLevel > 1) {
            isDragging = true;
            startX = event.clientX - offsetX;
            startY = event.clientY - offsetY;
            image.style.cursor = "grabbing";
        }
    });

    // Add mouse move event for dragging
    overlay.addEventListener("mousemove", (event) => {
        if (isDragging) {
            offsetX = event.clientX - startX;
            offsetY = event.clientY - startY;
            image.style.transform = `scale(${zoomLevel}) translate(${offsetX / zoomLevel}px, ${offsetY / zoomLevel}px)`;
        }
    });

    // Add mouse up event to stop dragging
    overlay.addEventListener("mouseup", () => {
        isDragging = false; // Stop dragging on mouse release
        image.style.cursor = "grab";
    });

    // Add mouse leave event to stop dragging when leaving the overlay
    overlay.addEventListener("mouseleave", () => {
        isDragging = false; // Ensure dragging stops if the cursor leaves the overlay
        image.style.cursor = "grab";
    });

    // Create close button
    const closeButton = document.createElement("button");
    closeButton.textContent = "X";
    closeButton.style.position = "absolute";
    closeButton.style.top = "20px";
    closeButton.style.right = "20px";
    closeButton.style.backgroundColor = "white";
    closeButton.style.color = "black";
    closeButton.style.border = "none";
    closeButton.style.padding = "10px 15px";
    closeButton.style.cursor = "pointer";
    closeButton.style.fontSize = "20px";
    closeButton.style.fontWeight = "bold";
    closeButton.style.zIndex = "1001";

    // Add close functionality
    closeButton.addEventListener("click", () => {
        document.body.removeChild(overlay);
    });

    // Append elements to overlay
    overlay.appendChild(image);
    overlay.appendChild(closeButton);

    // Append overlay to body
    document.body.appendChild(overlay);
}


