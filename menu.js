document.addEventListener('DOMContentLoaded', function () {
    const menuButton = document.getElementById('menuButton');
    const popupMenu = document.getElementById('popupMenu');

    menuButton.addEventListener('click', function () {
        popupMenu.classList.toggle('show');
    });
});
